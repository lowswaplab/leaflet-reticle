
# leaflet-reticle

[Leaflet](https://leafletjs.com/) Reticle Plugin

# NPM

This package is available on [npm](https://www.npmjs.com/):

[@lowswaplab/leaflet-reticle](https://www.npmjs.com/package/@lowswaplab/leaflet-reticle)

This package can be installed by performing:

    npm install @lowswaplab/leaflet-reticle

# JavaScript Example:

    import "@lowswaplab/leaflet-reticle";

    L.reticle().addTo(map);

Options:

    L.reticle(
      {
      color:      "#FFF",   // text color
      fontSize:   "16pt",   // text font size
      text:       "+",      // text
      textShadow: "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000", // text shadow (outline)
      zIndex:     1000      // reticle z-index
      }).addTo(map);

# Source Code

[leaflet-reticle](https://gitlab.com/lowswaplab/leaflet-reticle)

# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).

# Author

[Low SWaP Lab](https://www.lowswaplab.com/)

# Copyright

Copyright © 2020 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

