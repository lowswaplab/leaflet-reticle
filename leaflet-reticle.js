
// Leaflet Reticle plugin

// https://gitlab.com/lowswaplab/leaflet-reticle

// Copyright © 2020 Low SWaP Lab lowswaplab.com

"use strict";

(function()
  {
  L.Reticle = L.Layer.extend(
    {
    options:
      {
      color: "#FFF",
      fontSize: "16pt",
      text: "+",
      textShadow:
        "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000",
      // XXX how to I make this hide under tooltips and popups?
      zIndex: 1000
      },

    initialize: function(options)
      {
      L.setOptions(this, options);
      },

    onAdd: function(map)
      {
      var span;
      var height;
      var width;

      this._container = L.DomUtil.create("div", "leaflet-reticle");
      this._container.setAttribute("id", "leaflet-reticle");
      this._container.style.position = "relative";
      this._container.style.top = "50%";
      this._container.style.left = "50%";
      this._container.style.zIndex = this.options.zIndex;
      this._container.style.pointerEvents = "none";

      span = L.DomUtil.create("span", "leaflet-reticle-span");
      span.setAttribute("id", "leaflet-reticle-span");
      span.innerHTML = this.options.text;
      span.style.fontSize = this.options.fontSize;
      span.style.color = this.options.color;
      span.style.textShadow = this.options.textShadow;

      this._container.appendChild(span);
      map._container.appendChild(this._container);

      // make sure reticle text is centered
      // have to do this after adding to map
      height = span.offsetHeight / -2;
      width = span.offsetWidth / -2;
      this._container.style.margin =
        height.toFixed(0) + "px 0 0 " + width.toFixed(0) + "px";
      },

    addTo: function(map)
      {
      map.addLayer(this);

      return(this);
      },

    onRemove: function(map)
      {
      map._container.removeChild(this._container);
      },

    setzIndex(index)
      {
      this.options.zIndex = index;
      this._container.style.zIndex = this.options.zIndex;
      }
    });

  L.reticle = function(options)
    {
    return(new L.Reticle(options));
    };
  }
)();

